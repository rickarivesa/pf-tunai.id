Rules of Firewall

* Blok semua komunikasi data dari dan menuju server
* Ijinkan akses menuju repository FreeBSD
* Ijinkan komunikasi data berbasis http / https
* Pembatasan kegagalan user dalam ssh
* Mengijinkan koneksi ssh dengan maksimal koneksi 10 koneksi dari setiap single host
* Membatasi koneksi pada port 80, 443 dengan maksimal rate 500 koneksi per detik.
* Hanya mengijinkan icmp request dengan valid format 
* Membatasi jumlah koneksi tcp dan udp yaitu maksimal 10 koneksi dari setiap host dengan kecepatan maksimal 10 koneksi/detik